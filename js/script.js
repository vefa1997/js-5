function openTabs(evt, tabCont) {
    let i, contentTab, tabs_title;
    contentTab = document.getElementsByClassName("contentTab");
    for (i = 0; i < contentTab.length; i++) {
        contentTab[i].style.display = "none";
    }
    tabs_title = document.getElementsByClassName("tabs_title");
    for (i = 0; i < tabs_title.length; i++) {
        tabs_title[i].className = tabs_title[i].className.replace(" active", "");
    }
    document.getElementById(tabCont).style.display = "block";
    evt.currentTarget.className += " active";
}



